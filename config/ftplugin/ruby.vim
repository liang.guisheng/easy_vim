set ts=2
set sw=2


" ruby 编辑时Tab跳过特殊字符
inoremap <buffer> <Tab> <c-r>=SkipSpecialChars("[\\]\\)\\}>'`;\"]")<C-J>



" ruby 的单行注释
inoremap <buffer> <c-_> <c-r>=ScriptCommentToggle()<C-J><Esc>
nnoremap <buffer> <c-_> i<c-r>=ScriptCommentToggle()<C-J><Esc>
" ruby 的多行注释
vnoremap <buffer> <c-_> <Esc>i<c-r>=ScriptVCommentToggle()<C-J><Esc>x
