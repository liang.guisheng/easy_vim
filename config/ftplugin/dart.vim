inoremap <buffer> <Tab> <c-r>=SkipSpecialChars("[\\]\\)\\}>'`;\"]")<C-J>
set colorcolumn=120

nnoremap ,r :!dart % 




if has('nvim')
    nnoremap <buffer> <C-M-l> mc:DartFmt<CR>'c
"    vnoremap <buffer> <C-M-l> mc:0,$!yapf<CR>'c
"    inoremap <buffer> <C-M-l> <Esc>mc:0,$!yapf<CR>'c
else
    nnoremap <buffer> <C-S-l> mc:DartFmt<CR>'c
"    vnoremap <buffer> <C-S-l> mc:0,$!yapf<CR>'c
"    inoremap <buffer> <C-S-l> <Esc>mc:0,$!yapf<CR>'c
endif


" go 的单行注释
inoremap <buffer> <c-_> <c-r>=CStyleCommentToggle(".")<C-J><Esc>
nnoremap <buffer> <c-_> i<c-r>=CStyleCommentToggle(".")<C-J><Esc>
" go 的多行注释
vnoremap <buffer> <c-_> <Esc>i<c-r>=CStyleVCommentToggle()<C-J><Esc>x


set tabstop=2
set softtabstop=2
set shiftwidth=2
