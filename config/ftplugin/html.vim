inoremap <buffer> <Tab> <c-r>=SkipSpecialChars("[\\]\\)\\}>'`;\"]")<C-J>

"单行注释
inoremap <buffer> <c-_> <c-r>=XhtmlLineCommentToggle()<C-J><Esc>
nnoremap <buffer> <c-_> i<c-r>=XhtmlLineCommentToggle()<C-J><Esc>
"多行注释
vnoremap <buffer> <c-_> <Esc>i<c-r>=XhtmlBlockCommentToggle()<C-J><Esc>





