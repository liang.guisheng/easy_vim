set colorcolumn=120

"修改缩进为2个空格
set expandtab
set shiftwidth=2
set softtabstop=2

" go 编辑时Tab跳过特殊字符
inoremap <buffer> <Tab> <c-r>=SkipSpecialChars("[\\]\\)\\}>'`;\"]")<C-J>

" go 的单行注释
inoremap <buffer> <c-_> <c-r>=CStyleCommentToggle(".")<C-J><Esc>
nnoremap <buffer> <c-_> i<c-r>=CStyleCommentToggle(".")<C-J><Esc>
" go 的多行注释
vnoremap <buffer> <c-_> <Esc>i<c-r>=CStyleVCommentToggle()<C-J><Esc>x


"在当前文件中使用"."来自动出发自动补全功能
inoremap <buffer> . .<C-x><C-o>



