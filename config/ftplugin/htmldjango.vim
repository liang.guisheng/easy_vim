inoremap <buffer> <Tab> <c-r>=SkipSpecialChars("[\\]\\)\\}>'`;\"]")<C-J>
inoremap <buffer> <c-_> <c-r>=XhtmlLineCommentToggle()<C-J><Esc>x
nnoremap <buffer> <c-_> i<c-r>=XhtmlLineCommentToggle()<C-J><Esc>x
vnoremap <buffer> <c-_> <Esc>i<c-r>=XhtmlBlockCommentToggle()<C-J><Esc>x

set iskeyword+=-
