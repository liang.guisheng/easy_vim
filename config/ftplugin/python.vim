" python 编辑时Tab跳过特殊字符
inoremap <buffer> <Tab> <c-r>=SkipSpecialChars("[\\]\\)\\}>'`;\"]")<C-J>

" python 的单行注释
inoremap <buffer> <c-_> <c-r>=ScriptCommentToggle()<C-J><Esc>
nnoremap <buffer> <c-_> i<c-r>=ScriptCommentToggle()<C-J><Esc>
" python 的多行注释
vnoremap <buffer> <c-_> <Esc>i<c-r>=ScriptVCommentToggle()<C-J><Esc>x

set colorcolumn=120

" google python格式化器
"if has('nvim')
"    nnoremap <buffer> <C-M-l> mc:0,$!yapf<CR>'c
"    vnoremap <buffer> <C-M-l> mc:0,$!yapf<CR>'c
"    inoremap <buffer> <C-M-l> <Esc>mc:0,$!yapf<CR>'c
"else
"    nnoremap <buffer> <C-S-l> mc:0,$!yapf<CR>'c
"    vnoremap <buffer> <C-S-l> mc:0,$!yapf<CR>'c
"    inoremap <buffer> <C-S-l> <Esc>mc:0,$!yapf<CR>'c
"endif

abbreviate false False
abbreviate true True

nnoremap ,rp :!python %
nnoremap ,rpp :!pdm run %




" pymode 的格式化器
"autocmd FileType python nnoremap <C-f> :PymodeLintAuto<CR>
"let g:pymode_options_max_line_length=120





"python ale 相关的设置
"########################################
"let g:ale_sign_column_always = 1
"let g:ale_set_highlights = 0
let g:airline#extensions#ale#enabled=1

"自定义error和warning图标
let g:ale_sign_error = 'x'
let g:ale_sign_warning = '!'
"highlight ALEErrorSign ctermbg=NONE ctermfg=Red
"highlight ALEWarningSign ctermbg=NONE ctermfg=Yellow

let g:ale_statusline_format = ['✗ %d', '⚡ %d', '✔ OK']

"显示Linter名称,出错或警告等相关信息
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
"########################################


