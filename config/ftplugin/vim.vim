" vim 编辑时Tab跳过特殊字符
inoremap <buffer> <Tab> <c-r>=SkipSpecialChars("[\\]\\)\\}>'`;\"]")<C-J>
" vim 的单行注释
inoremap <buffer> <c-_> <c-r>=VimCommentToggle(".")<C-J><Esc>x
nnoremap <buffer> <c-_> i<c-r>=VimCommentToggle(".")<C-J><Esc>x
" vim 的多行注释
vnoremap <buffer> <c-_> <Esc>i<c-r>=VimCommentToggle()<C-J><Esc>x
