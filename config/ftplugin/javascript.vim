" javascript 编辑时Tab跳过特殊字符
inoremap <buffer> <Tab> <c-r>=SkipSpecialChars("[\\]\\)\\}>'`;\"]")<C-J>

set expandtab
set shiftwidth=1
set softtabstop=1
set tabstop=1
