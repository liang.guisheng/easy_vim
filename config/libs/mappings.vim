
"Vim 窗口导航键映射
"########################################
map <c-h> <c-w>h
map <c-l> <c-w>l
map <c-j> <c-w>j
map <c-k> <c-w>k
"行编辑时的跳转
nmap ,b 0
nmap ,e $

" 置换分号和冒号的功能
nnoremap ; :
nnoremap : ;

"行编辑时的删除
nnoremap de d$
nnoremap db d0
nnoremap dd dd

inoremap jj <Esc>
inoremap jk <Esc>
inoremap kj <Esc>
inoremap kk <Esc>
inoremap jl <Esc>

"保存退出相关
nnoremap ,w :w<CR>
nnoremap ,wq :wq<CR>
nnoremap ,wqa :wqa<CR>
inoremap ,w <Esc>:w<CR>
inoremap ,wq <Esc>:wq<CR>
inoremap ,wqa <Esc>:wqa<CR>
nnoremap ,q :q<CR>
nnoremap ,qq :q!<CR>
nnoremap ,qa :qa<CR>


nnoremap ,vs :vs<CR>
nnoremap ,hs :split<CR>

"将单词转换为小写字母
nnoremap gle gu$
nnoremap glb gu^
nnoremap glw guw


"将单词转换为大写字母
nnoremap gue gU$
nnoremap gub gU^
nnoremap guw gUw

inoremap ( ()<Esc>i
inoremap [ []<Esc>i
inoremap { {}<Esc>i
inoremap < <><Esc>i

inoremap ) <c-r>=CloseBracket(')')<CR>
inoremap ] <c-r>=CloseBracket(']')<CR>
inoremap > <c-r>=CloseBracket('>')<CR>
inoremap } <c-r>=CloseBracket('}')<CR>

inoremap ' ''<Esc>i
inoremap " ""<Esc>i
inoremap ` ``<Esc>i

"inoremap ' <c-r>=CloseBracket("'")<CR>
"inoremap " <c-r>=CloseBracket('"')<CR>
"inoremap ` <c-r>=CloseBracket('`')<CR>

inoremap <BS> <c-r>=SmartPairDelete()<CR>



nnoremap ,cl :!clear<CR>


au BufNewFile,BufRead *.py setf python
au BufNewFile,BufRead *.html,*.htm,*.xml setf html 
autocmd FileType html,css,htmldjango EmmetInstall

augroup shell
    autocmd!
    autocmd FileType sh nnoremap <buffer>,rl :exec '!'.getline('.')<CR>
augroup end

augroup python
    autocmd!
    autocmd FileType python nnoremap <buffer>,r :!python3 %
augroup end

