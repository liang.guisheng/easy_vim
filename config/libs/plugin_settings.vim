"vimfiler 配置
let g:vimfiler_as_default_explorer = 1
nnoremap <c-\> :VimFilerExplorer<CR> " 使用 ctrl+\来打开和关闭文件管理器
let g:vimfiler_safe_mode_by_default = 0 " 关闭安全模式可以操作文件和目录
let g:vimfiler_ignore_pattern='^\%(\.git\|\.DS_Store\|\.idea\|\.vscode\|.pytest_cache\|.tox\|__pycache__\)$'

" tagbar配置
nnoremap ,tl :TagbarToggle<CR>


"CtrlP 设置
let g:ctrlp_map = '<c-f>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'

set wildignore+=*.pyc
let g:ctrlp_custom_ignore = {  'dir':  '\v[\/]\.(git|hg|svn)|__pycache__$',  'file': '\v\.(exe|so|dll|pyc|jpg|jpeg|dat)$',  'link': 'some_bad_symbolic_links',  }



"########################################"
"启用 air-powerline for vim
"########################################
set laststatus=2
set noshowmode  "隐藏默认的模式文本

"enable use airline sybmols"
let g:airline_powerline_fonts = 1 

"enable smater tab line
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#clock#format = '%H:%M:%S'
let g:airline#extensions#clock#updatetime = 1000 "milliseconds 




" deoplete.vim 配置
let g:deoplete#enable_at_startup = 1
let g:python3_host_prog='/home/liang/.pyenv/shims/python'
"let g:python_host_prog='/usr/bin/python2.7'



" python 格式化器 yapf

" 让 syntastic 插件对python文件的保存和退出不做检查
"autocmd FileType python let g:syntastic_check_on_wq = 0


" 让 pymode 使用python3 的语法检查
if has('nvim')
    let g:pymode_python = 'python3'
else
    let g:pymode_python = 'python2'
endif

let g:pymode_lint = 1
let g:pymode_lint_on_write = 0
let g:pymode_lint_on_fly = 1

" snippets 相关配置
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"




"Emmet 设置
"始终开启标志列
let g:user_emmet_install_global = 0
let g:user_emmet_leader_key='<C-e>'


"markdown preview configuration0
"let vim_markdown_preview_toggle=1

"Vim-go 设置
let g:go_version_warning=0


"vim jsx pretty settings
let g:vim_jsx_pretty_highlight_close_tag=1

"Rainbow parenthese 设置
""let g:rainbow#max_level=16
""let g:rainbow#pairs=[['(',')'], ['[', ']'], ['{','}']]
""let g:rainbow#blacklist=[233, 234]
let g:rbpt_max = 16
let g:rbpt_loadcmd_toggle = 0
let g:bold_parentheses = 1

au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces
au Syntax * RainbowParenthesesLoadChevrons

let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

"Rainbow settings
"let g:rainbow_active=1
"let g:rainbow_conf = {
"\    'ctermfgs': ['lightblue', 'lightyellow', 'lightcyan', 'lightmagenta'],
"\    'cterms': [''],
"\    'operators': '_,_',
"\    'parentheses': ['start=/(/ end=/)/ fold', 'start=/\[/ end=/\]/ fold', 'start=/{/ end=/}/ fold']
"\ }


"let vim gitgutter refresh mark after save
autocmd BufWritePost * GitGutter


" rest client configuration
let g:vrc_output_buffer_name = '__VRC_OUTPUT.json'

" vim http client configuration to avoid encode error for json payload
let http_client_json_escape_utf=0

" dart lsc configuration
let g:lsc_auto_map = v:true

"git messenger configuration
nmap ,gm <Plug>(git-messenger)

let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'
let g:ale_lint_on_text_changed = 'never'
