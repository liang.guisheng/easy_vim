"vim 个人定制的一些函数

"########################################
"在编辑代码的时候跳过一些特殊的字符
"########################################
function! SkipSpecialChars(pattern)
    let line=getline('.')
    let column = col('.')
    let target = line[column - 1]
    if match(target, a:pattern) < 0
        let value = UltiSnips#ExpandSnippetOrJump()
        echo value
        return value
    else
        return "\<Right>"
    endif
endfunction



"########################################
"避免键入右括号
"########################################
function! CloseBracket(char)
    if getline('.')[col('.') -1] == a:char
        return "\<Right>"
    else
        return a:char
    endif
endfunction



"########################################
"检查当前系统的时间是否为白天
"########################################
function! IsDay()
    let current_hour = strftime("%H")
    if current_hour >6
        if current_hour < 18
            return 1
        endif
    endif
    return 0
endfunction

"########################################
" xhtml 文件类型的单行注释
"########################################
function! XhtmlLineCommentToggle()
    let line = getline('.')
    " 已注释的行
    if line =~ '^\s*<!--'
        return "\<Esc>$%df $F d$"
    else
        return "\<Esc>^i<!-- \<Esc>$a -->\<Esc>"
    endif
endfunction


"########################################
" xhtml 文件类型的多行注释
"########################################
function! XhtmlBlockCommentToggle()
    let [row1, col1] = getpos("'<")[1:2]
    let [row2, col2] = getpos("'>")[1:2]
    if row1 != row2 
        let line1 = getline(row1)
        let line2 = getline(row2) 
        
        if line1=~ '^<!--' && line2 =~ '-->$'
            let comment_start_index=stridx(line1,'<!--')
            let comment_new_start_line=strpart(line1,comment_start_index+4)
            call setline(row1,comment_new_start_line)

            let comment_end_index=stridx(line2,'-->')
            let comment_new_end_line=strpart(line2,0,comment_end_index)
            call setline(row2, comment_new_end_line) 
        else
            call setline(row1, "<!-- ".line1)
            call setline(row2,line2." -->")
            echo line2
        endif

    else
        let line=getline(row1) 
        
        call setline(row1,"<!-- ".line." -->") 
    endif
endfunction


"Linux 脚本文件单行注释开关--在插入模式中
function! ScriptCommentToggle()
	let line=getline(".")
"    已注释的行
    if match(line,'^\s*#') < 0
        return "\<Esc>^i# \<Esc>"
    else
        if match(line, '#') == 0
            return "\<Esc>0df "
        else
            return "\<Esc>0f#df "
        endif
    endif
	
endfunction

"Linux 脚本文件多行注释-在插入模式中
function! ScriptVCommentToggle()

    let [row1, col1] = getpos("'<")[1:2]
    let [row2, col2] = getpos("'>")[1:2]
   
    for line_index in range(row1, row2)
        let line = getline(line_index)

        if match(line,'^\s*#') < 0
            execute "normal ".line_index."gg^i# "
        else
            if match(line, '#') == 0
                execute "normal ".line_index."gg02x"
            elseif match(line, '#') > 0
                execute "normal ".line_index."gg0f#2x"
            endif
        endif

    endfor
endfunction

"C语言风格的单行注释-在插入模式中
function! CStyleCommentToggle(row)
	let line=getline(a:row)

	if line =~ '^//'
        let comment_start_index = stridx(line, '//')
        let new_line = strpart(line, comment_start_index+2)
        call setline(a:row, new_line)
	else
	    call setline(a:row,'//'.line)
	endif
endfunction

"C语言风格的多行注释-在插入模式中
function! CStyleVCommentToggle()
    
    let [row1, col1] = getpos("'<")[1:2]
    let [row2, col2] = getpos("'>")[1:2]

    if row1 != row2

       for line_index in range(row1, row2)
           call CStyleCommentToggle(line_index) 
       endfor
    else
        call CStyleCommentToggle(row1)
    endif
    
endfunction

"Vim脚本语言的单行注释
function! VimCommentToggle(row)
	let line=getline(a:row)
	if line =~ '^"'
            let comment_start_index = stridx(line, '"')
            let new_line = strpart(line, comment_start_index+1)
            call setline(a:row, new_line)
	else
            call setline(a:row,'"'.line)	
	endif

endfunction

"Vim脚本语言的多行注释
function! VimVCommentToggle()

    let [row1, col1] = getpos("'<")[1:2]
    let [row2, col2] = getpos("'>")[1:2]
    
    if row1 == row2
        call  VimCommentToggle(row1)
    else
        
        for line_index in range(row1, row2)
           call VimCommentToggle(line_index) 
        endfor
    endif
endfunction







"########################################"
"执行当前的可执行脚本文件
"
"Support:
"
"   sh     script
"   zsh    script
"   python script
"   perl   script
"########################################
function! RunCurrentScript()

    if &filetype == "sh"
       execute "!bash %" 
    elseif &filetype == "zsh"
        execute "!zsh %"
    elseif &filetype == "python"
        execute "!python %"
    elseif &filetype == "perl"
        execute "!perl %"
    else
        echo "filetype : ".&filetype." is not an recogonised script file"
    endif
endfunction


function! RunCurrentScriptLine()
    execute ". !bash"
endfunction

"##############################
"删除配对符号的时候，如果配对符号中间为空，则自动删除整个配对
"##############################
function! SmartPairDelete()
    
    let line  = getline(".")
    let column = col(".")
    let current_char = line[column-2]
    let previous_char = line[column - 3]
    let next_char  =line[column - 1]

    let delete_begin_command="\<BS>\<RIGHT>\<BS>"
    let delete_end_command="\<BS>\<BS>"

    let begin_chars = ["(", "[", "<", "{", "'", "`", "\""] 
    let end_chars = [")", "]", ">", "}", "'", "`", "\""]

"    如果当前删除的字符是配对符号的开头，则删除当前字符和之后字符
    if index(begin_chars, current_char) >= 0
        if index(end_chars, next_char) == index(begin_chars, current_char)
            return delete_begin_command
        endif 
    endif

"    如果当前删除字符是配对符号的结尾，则删除当前字符和之前的字符
    if index(end_chars, current_char) >= 0
        if index(begin_chars, previous_char) == index(end_chars, current_char)
            return delete_end_command
        endif
    endif

    return "\<BS>"
    
endfunction

