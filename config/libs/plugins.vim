"vim 相关的统一配置文件
"兼容neovim & vim
if has('nvim')
    call plug#begin('~/.local/share/nvim/plugged')
else
    call plug#begin('~/.local/share/vim/plugged')
endif

"文件管理器
Plug 'Shougo/unite.vim'
Plug 'Shougo/vimfiler.vim'
Plug 'junegunn/vim-easy-align'
Plug 'majutsushi/tagbar'


" git 支持
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

"颜色配置
Plug 'rafi/awesome-vim-colorschemes'
Plug 'jnurmine/Zenburn'
Plug 'altercation/vim-colors-solarized'
Plug 'pangloss/vim-javascript'

"文件查找
Plug 'ctrlpvim/ctrlp.vim'

"引号括号包含
Plug 'tpope/vim-surround'


"状态栏显示
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'enricobacis/vim-airline-clock'

"代码格式化
Plug 'nvie/vim-flake8'


" 代码语法支持
"Plug 'vim-syntastic/syntastic' "语法检查 plugin, python-mode包含了Python的语法支持

" Python 支持
Plug 'w0rp/ale' "异步代码lint插件
Plug 'zchee/deoplete-jedi'
Plug 'google/yapf' "代码格式化器， 在 pymode 种已经有支持了
Plug 'davidhalter/jedi-vim'
"Plug 'jmcantrell/vim-virtualenv'

"兼容 neovim & vim
"if has('nvim')
"    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"else
"    Plug 'Shougo/deoplete.nvim'
"    Plug 'roxma/nvim-yarp'
"    Plug 'roxma/vim-hug-neovim-rpc'
"endif

""Plug 'python-mode/python-mode', { 'branch': 'develop' }
Plug 'vim-scripts/indentpython.vim'
"Plug 'lambdalisue/vim-pyenv' " Python 虚拟环境支持， 在 python-mode 中已经有支持了
"Plug 'nvie/vim-flake8' " PEP8 格式检查

" 代码片段支持
Plug 'SirVer/ultisnips'
"Plug 'honza/vim-snippets'
Plug 'theo-l/vim-snippets'

Plug 'mattn/emmet-vim'

"markdown plugin
"Plug 'JamshedVesuna/vim-markdown-preview'

"彩虹括号匹配
"Plug 'kien/rainbow_parentheses.vim'
"Plug 'junegunn/rainbow_parentheses.vim'
Plug 'eapache/rainbow_parentheses.vim'
"Plug 'luochen1990/rainbow'
Plug 'maxmellon/vim-jsx-pretty'
"
"Golang plugin
"Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

" Auto complete plugin
" Plug 'zxqfl/tabnine-vim'

" Rest client for vim
"Plug 'diepm/vim-rest-console'
Plug 'aquach/vim-http-client'

" dart plugins
Plug 'dart-lang/dart-vim-plugin'
Plug 'natebosch/vim-lsc'
Plug 'natebosch/vim-lsc-dart'
Plug 'thosakwe/vim-flutter'

" graphql
"Plug 'jparise/vim-graphql'

" manager of the vim tab/buffers
Plug 'mg979/vim-xtabline'

" 
"Plug 'liuchengxu/vim-which-key'

" Show a good look startup UI
Plug 'mhinz/vim-startify'

"vim distract mode, I don't like too much
"Plug 'junegunn/goyo.vim'

Plug 'rhysd/git-messenger.vim'

" interactive code
Plug 'metakirby5/codi.vim'

" for white space display"
Plug 'ntpeters/vim-better-whitespace'

" yaml"
"Plug 'stephpy/vim-yaml'

"Plug 'neoclide/coc.nvim', {'branch': 'release'}


" Rust
Plug 'rust-lang/rust.vim'

call plug#end()

