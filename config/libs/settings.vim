"文件类型以及语法高亮

syntax on
filetype plugin on 
filetype indent on
set fileencoding=utf-8

" 编辑器显示
set number
set incsearch
set hlsearch
set ruler
set showcmd
if has('nvim')
    set termguicolors
endif

"使用随机的颜色方案
let themes = ["one" , "parsec" , "scheakur" , "solarized8_high" , "termschool" , "abstract" , "apprentice" , "challenger_deep" , "flattened_light" , "gruvbox" , "iceberg" , "materialbox" , "nord" , "orange-moon" , "pink-moon" , "seoul256-light" , "solarized8_low" , "twilight256" , "afterglow" , "archery" , "deep-space" , "focuspoint" , "happy_hacking" , "jellybeans" , "meta5" , "OceanicNextLight" , "orbital" , "pyte" , "seoul256" , "solarized8" , "two-firewatch" , "alduin" , "atom" , "deus" , "github" , "hybrid_material" , "lightning" , "minimalist" , "OceanicNext" , "PaperColor" , "rakr" , "sierra" , "space-vim-dark" , "wombat256mod" , "anderson" , "carbonized-dark" , "dracula" , "gotham256" , "hybrid_reverse" , "lucid" , "molokai" , "onedark" , "paramount" , "rdark-terminal2" , "solarized8_flat" , "tender" , "yellow-moon","blue", "darkblue", "default", "delek", "desert", "elflord", "evening", "industry", "koehler", "morning", "murphy", "pablo", "peachpuff", "ron", "shine", "slate", "torte", "zellner"]

let prefer_colorschemes = ['lightning', 'hybrid_reverse', 'hybrid_material', 'hybrid', 'happy_hacking','one','onedark', 'elflord', 'dracula', 'scheakur','molokai', 'flattened_light', 'anderson', 'afterglow','gruvbox', 'seoul256', 'tender', 'murphy', 'PaperColor', 'abstract', 'jellybeans', 'deus', 'torte', 'rakr', 'github', 'paramount'] 
execute 'colorscheme '.prefer_colorschemes[localtime() % len(prefer_colorschemes)]
unlet themes

set wrap
set wildmenu
set clipboard=unnamedplus

set history=1000

set expandtab

set autoindent
set tabstop=4
set softtabstop=4
set shiftwidth=4

set ignorecase
set smartcase
set mouse=a
set mousehide
set linespace=0
set fileformats=unix,mac,dos
set cursorline
set cursorcolumn
set showmatch
set relativenumber

"gui 相关设置"
set guifont=Source\ Code\ Pro\ for\ Powerline\ Regular\ 10 "设置gvim的字体"
"set guifont=Monaco\ 10 "设置gvim的字体"
set guioptions-=m "去掉菜单栏"
set guioptions-=T "去掉工具栏"



"FIXME:高亮组名称 BadWhitespace 好像找不到，需要解决 
" au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

au BufNewFile,BufRead *.py;
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4
    \ set textwidth=119
    \ set expandtab
    \ set autoindent
    \ set fileformat=unix


au BufNewFile,BufRead *.js;
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2


au BufNewFile,BufRead *.html;
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2



au BufNewFile,BufRead *.css;
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2

