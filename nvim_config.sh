#/bin/bash
set -eu


__nvim_config () {

    echo "starting nvim configuration"

    current_dir=$(pwd)

    if [ -e ~/.vim ]; then
	echo "remove old .vim and replace it with new"
        rm -rf ~/.vim
    fi
    echo "create .vim config dir"
    ln -s "$current_dir/config" ~/.vim

    if [ -e ~/.config/yapf ]; then
	echo "remove old yapf and replace it with new"
        rm -rf  ~/.config/yapf
    fi
    echo "create yapf config dir"
    ln -s "$current_dir/config/yapf" ~/.config/yapf

    if [ -e ~/.config/nvim ]; then
	echo "remove old nvim config and replace it with new"
        rm -rf ~/.config/nvim
    fi
    echo "create nvim config dir"
    ln -s "$current_dir/config" ~/.config/nvim

}

__nvim_config
