#!/bin/bash
set -eu

__vim_config() {

    if [[ -z $(which vim) ]]; then
        sudo apt install vim
    fi

    current_dir=$(pwd)

    if [[ -e ~/.vim ]]; then
        rm -rf ~/.vim
        ln -s "$current_dir/config" ~/.vim
    fi

    if [[ -e ~/.config/yapf ]]; then
        rm ~/.config/yapf
        ln -s "$current_dir/config/yapf"  ~/.config/yapf
    fi 

    cp "$current_dir/config/init.vim" ~/.vimrc

    vim +PlugInstall

}

__vim_config