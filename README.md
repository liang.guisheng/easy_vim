# easy_vim

make vim configuration easier

## Usage

```
git clone https://gitlab.com/theo-l/easy_vim

cd easy_vim

./vim_config.sh
```
